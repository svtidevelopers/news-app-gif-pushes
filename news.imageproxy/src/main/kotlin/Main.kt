import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.sun.imageio.plugins.gif.GIFImageReader
import com.sun.imageio.plugins.gif.GIFImageReaderSpi
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.response.respondBytes
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.io.*
import java.net.URLDecoder
import java.time.Duration
import javax.imageio.ImageReader
import kotlin.collections.HashMap
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import java.awt.Color
import javax.imageio.metadata.IIOMetadataNode
import java.awt.Graphics2D
import java.util.ArrayList
import java.io.IOException

val cache = HashMap<String, ExplodedGif>()
fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(CORS) {
        maxAge = Duration.ofDays(1)
        anyHost()
        method(HttpMethod.Get)
    }
    install(Routing) {
        get("/zip/{zipName}"){
            val zipName = call.parameters["zipName"]
            val file = File("image", zipName)
            file.listFiles().filter { it.name.endsWith("gif") }.forEach { it.delete() }
            val zip = File(file, "zip.zip")
            val out = ZipOutputStream(FileOutputStream(zip))
            file.listFiles().filter{it.name.endsWith("png")}.forEach {
                out.putNextEntry(ZipEntry("image/"+zipName+"/"+it.name))
                try {
                    val bytes = File(file, it.name).readBytes()
                    out.write(bytes)
                    /*
                    val inputStream = FileInputStream(File(file, it.name))
                    val readBuffer = ByteArray(2048)
                    var amountRead: Int
                    var written = 0

                    do  {
                        amountRead = inputStream.read(readBuffer)
                        if(amountRead < 0) break
                        out.write(readBuffer, 0, amountRead)
                        written += amountRead
                    } while(amountRead > 0)
                    */
                }catch(t: Throwable){
                    t.printStackTrace()
                }
                out.closeEntry()
            }
            out.close()
            call.respondBytes(zip.readBytes())
        }
        get("/image/{id}/{imageName}"){
            val dirName = call.parameters["id"]
            val imageName = call.parameters["imageName"]

            val f = File(dirName, imageName)

            call.respondBytes(f.readBytes(), ContentType.Image.PNG)
        }
        get("/{imageUrl}"){
            val url = call.parameters["imageUrl"]
            val path = URLDecoder.decode(url ?: "", "UTF-8")
            val name = path.split("/").last()
            if(cache.containsKey(name)){
                call.respondText(Gson().toJson(cache[name]), ContentType.Application.Json)
                return@get
            }
            val result = explodeGif(path)
            cache[name] = result
            call.respondText(Gson().toJson(result), ContentType.Application.Json)
            //convert angrycat.gif -scene 1 +adjoin  frame_%03d.png
        }
    }
}

fun explodeGif(path: String): ExplodedGif {
    val (_, response, result) = path.httpGet().responseString()
    when (result) {
        is Result.Failure -> {
            println("request: $path FAILED $path")
        }
        is Result.Success -> {
            if (response.statusCode == 200) {
               return splitImage(path.split("/").last(), response.data)
            } else {
                println("FAILED $response.statusCode request: $path")
            }
        }
    }
    return ExplodedGif(0, 0, "", emptyList())
}

fun gif2png(input: File): String{
    val name = input.name.replace(Regex("\\.gif"), "")
    val output = File(input.parent, "$name.png")
    ImageIO.write(ImageIO.read(input), "png", output)
    return output.name
}
data class ExplodedGif(val width: Int, val height: Int, val zipName: String, val images: List<String>)
fun splitImage(name: String, data: ByteArray): ExplodedGif{
    val ret = mutableListOf<String>()
    var width = 0
    var height = 0
    try {
        val imageatt = arrayOf("imageLeftPosition", "imageTopPosition", "imageWidth", "imageHeight")

        val reader = ImageIO.getImageReadersByFormatName("gif").next() as ImageReader
        val ciis = ImageIO.createImageInputStream(ByteArrayInputStream(data))
        reader.setInput(ciis, false)

        val noi = reader.getNumImages(true)
        var master: BufferedImage? = null
        File("image").mkdir()
        val f = File("image", name).apply { mkdir() }
        for (i in 0 until noi) {
            val image = reader.read(i)
            val metadata = reader.getImageMetadata(i)

            val tree = metadata.getAsTree("javax_imageio_gif_image_1.0")
            val children = tree.childNodes

            for (j in 0 until children.length) {
                val nodeItem = children.item(j)

                if (nodeItem.nodeName.equals("ImageDescriptor")) {
                    val imageAttr = HashMap<String, Int>()

                    for (k in imageatt.indices) {
                        val attr = nodeItem.attributes
                        val attnode = attr.getNamedItem(imageatt[k])
                        imageAttr[imageatt[k]] = Integer.valueOf(attnode.nodeValue)
                    }
                    if (i == 0) {
                        width = imageAttr["imageWidth"]!!
                        height = imageAttr["imageHeight"]!!
                        master = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
                    }
                    master!!.graphics.drawImage(image, imageAttr["imageLeftPosition"]!!, imageAttr["imageTopPosition"]!!, null)
                }
            }
            ImageIO.write(master!!, "PNG", File(f,"$i.png"))
            ret.add("/image/" + name + "/" + File(f,"$i.png"))
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return ExplodedGif(width, height, "/zip/$name", ret)
}

@Throws(IOException::class)
private fun readGIF(reader: ImageReader): Array<ImageFrame> {
    val frames = ArrayList<ImageFrame>(2)

    var width = -1
    var height = -1

    val metadata = reader.streamMetadata
    if (metadata != null) {
        val globalRoot = metadata.getAsTree(metadata.nativeMetadataFormatName) as IIOMetadataNode

        val globalScreenDescriptor = globalRoot.getElementsByTagName("LogicalScreenDescriptor")

        if (globalScreenDescriptor != null && globalScreenDescriptor.length > 0) {
            val screenDescriptor = globalScreenDescriptor.item(0) as IIOMetadataNode

            if (screenDescriptor != null) {
                width = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenWidth"))
                height = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenHeight"))
            }
        }
    }

    var master: BufferedImage? = null
    var masterGraphics: Graphics2D? = null
    val ret = mutableListOf<String>()
    var frameIndex = 0
    while (true) {
        val image: BufferedImage
        try {
            image = reader.read(frameIndex)
        } catch (io: IndexOutOfBoundsException) {
            break
        }

        if (width == -1 || height == -1) {
            width = image.width
            height = image.height
        }

        val root = reader.getImageMetadata(frameIndex).getAsTree("javax_imageio_gif_image_1.0") as IIOMetadataNode
        val gce = root.getElementsByTagName("GraphicControlExtension").item(0) as IIOMetadataNode
        val delay = Integer.valueOf(gce.getAttribute("delayTime"))
        val disposal = gce.getAttribute("disposalMethod")

        var x = 0
        var y = 0

        if (master == null) {
            master = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
            masterGraphics = master.createGraphics()
            masterGraphics!!.background = Color(0, 0, 0, 0)
        } else {
            val children = root.childNodes
            for (nodeIndex in 0 until children.length) {
                val nodeItem = children.item(nodeIndex)
                if (nodeItem.nodeName.equals("ImageDescriptor")) {
                    val map = nodeItem.attributes
                    x = Integer.valueOf(map.getNamedItem("imageLeftPosition").nodeValue)
                    y = Integer.valueOf(map.getNamedItem("imageTopPosition").nodeValue)
                }
            }
        }
        masterGraphics!!.drawImage(image, x, y, null)

        val copy = BufferedImage(master.colorModel, master.copyData(null), master.isAlphaPremultiplied, null)
        frames.add(ImageFrame(copy, delay, disposal))

        if (disposal == "restoreToPrevious") {
            var from: BufferedImage? = null
            for (i in frameIndex - 1 downTo 0) {
                if (frames[i].disposal != "restoreToPrevious" || frameIndex == 0) {
                    from = frames[i].image
                    break
                }
            }

            master = BufferedImage(from!!.colorModel, from.copyData(null), from.isAlphaPremultiplied, null)
            masterGraphics = master.createGraphics()
            masterGraphics!!.background = Color(0, 0, 0, 0)
        } else if (disposal == "restoreToBackgroundColor") {
            masterGraphics.clearRect(x, y, image.width, image.height)
        }
        frameIndex++
      //  ret.add(width, height, "", ret)
    }
    reader.dispose()
    return frames.toTypedArray()
}

private class ImageFrame(val image: BufferedImage, val delay: Int, val disposal: String)

var port = 9000
fun main(args: Array<String>) {
    try {
        embeddedServer(Netty, System.getenv("PORT")?.toInt()
                ?: port, watchPaths = listOf("MainKt"), module = Application::module).start()
    } catch (t: Throwable) {
        error(t)
    }
}