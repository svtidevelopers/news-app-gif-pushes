##Supported image types
https://developer.android.com/guide/topics/media/media-formats

https://developers.google.com/speed/webp/faq

## RemoteViews
https://developer.android.com/reference/kotlin/android/widget/RemoteViews

https://developer.android.com/reference/android/appwidget/AppWidgetManager: The total Bitmap memory used by the RemoteViews object cannot exceed that required to fill the screen 1.5 times, ie. (screen width x screen height x 4 x 1.5) bytes.
Q. är det samma begränsning för notifikationer??

https://developer.android.com/guide/topics/ui/notifiers/notifications


## https://www.imagemagick.org/
Splitta upp gif i frames mha imagemagick
convert angrycat.gif -scene 1 +adjoin  frame_%03d.gif

convert angrycat.gif -scene 1 +adjoin  -coalesce frame_%03d.png

Info om en image
magick identify -verbose angrycat.gif

## zip
https://developer.android.com/reference/java/util/zip/package-summary

download zip, save to tmp and then unzip
next step is to unzip



## Aninmated webp
FrameSequence example library (https://android.googlesource.com/platform/frameworks/ex/+/master/framesequence) supports animated WebP and GIF.
The WebP code is here: https://android.googlesource.com/platform/frameworks/ex/+/master/framesequence/jni/FrameSequence_webp.cpp
and the example usage is here: https://android.googlesource.com/platform/frameworks/ex/+/master/framesequence/samples/FrameSequenceSamples/src/com/android/framesequence/samples/FrameSequenceTest.java


http://imagemagick.org/discourse-server/viewtopic.php?t=20041