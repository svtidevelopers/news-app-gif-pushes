package learn.asapehrsson.se.animatedpng

import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit


class ConfigureTimeouts @Throws(Exception::class)
constructor() {
    private val client: OkHttpClient

    init {
        client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
    }

    @Throws(Exception::class)
    fun run() {
        val request = Request.Builder()
                .url("http://httpbin.org/delay/2") // This URL is served with a 2 second delay.
                .build()

        client.newCall(request).execute().use { response -> println("Response completed: $response") }
    }

    companion object {

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            ConfigureTimeouts().run()
        }
    }
}