package learn.asapehrsson.se.animatedpng

import android.content.Context
import android.util.Log
import com.bumptech.glide.load.engine.GlideException
import com.google.gson.Gson
import okhttp3.*
import okio.Okio
import java.io.*
import java.net.URLEncoder
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

class PrepareExplodedGif(val context: Context,
                         var listener: (((ExplodedGifSpec) -> Unit))? = null) {
    private var imageDir: File? = null
    val client : OkHttpClient

    init {
        imageDir = File(context.filesDir, "the_files")
        if (imageDir?.exists() == false) {
            imageDir?.mkdirs()
        }
        client = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
    }

    fun fetchSpec(useZip: Boolean) {

        val url = MainActivity.HOST + "/" + URLEncoder.encode("https://news-api.static.svt.se/notification-assets/1534764414748_JustNu.gif")
        Log.d(TAG, "requesting spec: $url")
        client.newCall(Request.Builder()
                .url(url)
                .build()).enqueue(object : Callback {

            override fun onFailure(call: Call?, e: IOException?) {
                Log.e(TAG, "failed to get spec", e)
            }

            override fun onResponse(call: Call?, response: Response?) {
                try {
                    val string = response?.body()?.string()
                    val spec = Gson().fromJson<ExplodedGifSpec>(string, ExplodedGifSpec::class.java)
                    spec.base = MainActivity.HOST
                    if (useZip){
                        fetchZip(spec)
                    } else {
                        listener?.invoke(spec)
                    }

                } catch (e: Exception) {
                    Log.e(TAG, "Failed to fetch spec", e)
                }
            }
        })
    }

    fun fetchZip(spec: ExplodedGifSpec) {

        val zipUri = spec.getZipUri()
        Log.d(TAG, "will fetch zip: $zipUri")
        client.newCall(Request.Builder()
                .url(zipUri)
                .build()).enqueue(object : Callback {

            override fun onFailure(call: Call?, e: IOException?) {
                Log.e(TAG, "Failed to fetch zip", e)
            }

            override fun onResponse(call: Call?, response: Response?) {
                try {
                    val downloadedFile = File.createTempFile(UUID.randomUUID().toString(), null, imageDir)
                    val sink = Okio.buffer(Okio.sink(downloadedFile))
                    sink.writeAll(response?.body()!!.source())
                    sink.close()

                    unzip(downloadedFile, imageDir!!, spec)

                } catch (e: Exception) {
                    Log.e(TAG, "Failed to save zip files", e)
                }
            }
        })
    }

    fun unzip(zipFile: File, targetLocation: File, spec: ExplodedGifSpec) {
        //create target location folder if not exist

        try {
            val fin = FileInputStream(zipFile)
            val zin = ZipInputStream(BufferedInputStream(fin))
            var ze: ZipEntry? = zin.nextEntry
            while (ze != null) {
                //create dir if required while unzipping
                if (ze.isDirectory) {
                    mayCreateDir(targetLocation, ze.name)
                } else {
                    val file = File(targetLocation, ze.name)
                    Log.d(TAG, "saving: " + file.absolutePath + "" + ze.size)
                    val fout = FileOutputStream(file)
                    var c = zin.read()
                    while (c != -1) {
                        fout.write(c)
                        c = zin.read()
                    }

                    zin.closeEntry()
                    fout.close()
                }
                ze = zin.nextEntry
            }
            zin.close()
            listener?.invoke(ExplodedGifSpec(spec.width, spec.height, spec.images, spec.zipName, targetLocation.absolutePath.toString()))
            Log.d(TAG, "DONE saving files")
        } catch (e: Exception) {
            if (e is GlideException){
                e.logRootCauses("")
            }
            Log.e(TAG, "Failed to unzip", e)
        }

    }

    fun mayCreateDir(targetLocation: File, filepath: String) {
        val file = File(targetLocation, filepath)
        if (!file.exists()) {
            Log.d(TAG, "creating dir: " + file.absolutePath)
            file.mkdir()
        }
    }

    @Throws(IOException::class)
    fun onResponse(call: Call, response: Response) {
        val downloadedFile = File.createTempFile(UUID.randomUUID().toString(), null, context.cacheDir)
        val sink = Okio.buffer(Okio.sink(downloadedFile))
        sink.writeAll(response.body()!!.source())
        sink.close()
    }

    companion object {
        val TAG = PrepareExplodedGif::class.java.simpleName
        val BUFFER = 512
        val TOOBIG: Long = 0x64000 // Max size of unzipped data, 100MB
        val TOOMANY = 200      // Max number of files
    }
}