package learn.asapehrsson.se.animatedpng

class ExplodedGifSpec(val width: Int, val height: Int, val images: List<String>, val zipName: String, var base: String) {
    fun getZipUri() = normalize("$base/$zipName")

    fun getImageUri(index: Int): String {
        val file = images[index]
        return normalize("$base/$file")
    }

    fun normalize(string: String) = string.replace("//", "/").replaceFirst(":/", "://")
}