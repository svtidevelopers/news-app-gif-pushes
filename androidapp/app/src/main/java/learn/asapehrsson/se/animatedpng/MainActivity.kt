package learn.asapehrsson.se.animatedpng

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RemoteViews
import android.widget.ViewFlipper
import com.bumptech.glide.request.target.NotificationTarget
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)

        prepareViewFlipper(viewFlipper, params)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "description"
            getSystemService(NotificationManager::class.java)!!.createNotificationChannel(channel)
        }

        button.setOnClickListener {

            if (USE_SERVICE) {
                PrepareExplodedGif(this, ::createNotification).fetchSpec(true)
            } else {
                createNotification()
            }
        }
    }


    private fun createNotification(spec: ExplodedGifSpec? = null) {

        val notificationLayout = RemoteViews(packageName, R.layout.remote_view_small)
        val notificationLayoutExpanded = RemoteViews(packageName, R.layout.remote_view_large)

        val customNotification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setCustomBigContentView(notificationLayoutExpanded)
                .build()

        if (spec == null) {
            fun addView(remoteView: RemoteViews, id: Int) {
                val image = RemoteViews(packageName, R.layout.image_view)
                image.setImageViewResource(R.id.image, id)
                remoteView.addView(R.id.viewFlipper, image)
            }

            val imgs: TypedArray = resources.obtainTypedArray(R.array.random_imgs)
            for (i in 0..imgs.length()) {
                imgs.getResourceId(i, -1)
                addView(notificationLayoutExpanded, imgs.getResourceId(i, -1))
            }
            imgs.recycle();
        } else {
            fun addView(remoteView: RemoteViews, uriAsString: String, useGlide: Boolean = false, aspectRatio: Float = 1f) {
                val image = RemoteViews(packageName, R.layout.image_view)
                if (useGlide) {
                    // this one don't work. find out why!
                    Handler(Looper.getMainLooper()).post {
                        val notificationTarget = NotificationTarget(
                                this,
                                R.id.image,
                                image,
                                customNotification,
                                NOTIFICATION_ID);

                        GlideApp.with(this)
                                .asBitmap()
                                .load(uriAsString)
                                .into(notificationTarget)
                    }
                    remoteView.addView(R.id.viewFlipper, image)
                } else {
                    val screenWidth = getScreenWidth(this)
                    val file = File(uriAsString)
                    val fromHost = uriAsString.startsWith("http")
                    if (file.exists() || fromHost) {
                        Log.d(TAG, "loading " + file.absolutePath)
                        val theBitmap = GlideApp
                                .with(this)
                                .asBitmap()
                                .load(if (fromHost) uriAsString else file)
                                .submit(screenWidth, (screenWidth / aspectRatio).toInt())
                                .get()
                        image.setImageViewBitmap(R.id.image, theBitmap)
                        remoteView.addView(R.id.viewFlipper, image)

                        // this one don't work
                        // image.setImageViewUri(R.id.image, Uri.parse(uriAsString))
                    } else {
                        Log.d(TAG, "file dont exist! $uriAsString")
                    }
                }
            }

            val aspectRatio = spec.width / spec.height.toFloat()

            for ((index, image) in spec.images.withIndex()) {
                addView(notificationLayoutExpanded, spec.getImageUri(index), false, aspectRatio)
            }
        }

        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, customNotification)
    }


    private fun prepareViewFlipper(vf: ViewFlipper, params: ViewGroup.LayoutParams) {
        fun addView(viewFlipper: ViewFlipper, layoutParams: ViewGroup.LayoutParams, id: Int) {
            val imageView = ImageView(this)
            imageView.setImageResource(id)
            viewFlipper.addView(imageView, layoutParams)
        }

        val imgs: TypedArray = resources.obtainTypedArray(R.array.random_imgs)
        for (i in 0 until imgs.length()) {
            imgs.getResourceId(i, -1)
            addView(vf, params, imgs.getResourceId(i, -1))
        }
        imgs.recycle()
    }

    private fun getScreenWidth(context: Context): Int {
        val size = Point()
        (context as Activity).windowManager.defaultDisplay.getSize(size)
        return size.x
    }

    companion object {
        val TAG = MainActivity::class.java.simpleName

        val USE_SERVICE = true
        val HOST = "http://192.168.2.10:9000"
        val NOTIFICATION_ID = 1223
        val CHANNEL_ID = "123"
    }

}
